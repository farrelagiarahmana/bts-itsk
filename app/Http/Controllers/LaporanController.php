<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\file_laporan;
use App\Models\laporan;
use Illuminate\Support\Facades\Storage;

class LaporanController extends Controller
{
    public function index()
    {
        $laporans = Laporan::with(['kegiatan.users' => function ($query) {
            $query->where('jabatan', 'Ketua');
        }])->paginate(5);
        return view("admin.dashboard.laporan.laporan_index", compact("laporans"));
    }

    public function editForm($id)
    {
        $laporan = Laporan::findOrFail($id);
        return view("admin.dashboard.laporan.laporan_edit", compact("laporan"));
    }

    public function updateLaporan(Request $request, $id)
    {
        $request->validate([
            "status_promosi" => "required|in:Diterima,Diproses,Ditolak",
            'catatan' => 'nullable',
        ]);

        $laporan = Laporan::findOrFail($id);
        $laporan->update($request->all());

        return redirect()->route("admin.dashboard.laporan.laporan_index")->with("success", "Laporan berhasil diperbarui.");
    }
    public function deleteFile($id)
    {
        // Find the laporan by ID
        $laporan = Laporan::findOrFail($id);

        // Detach associated files and delete them
        foreach ($laporan->files as $file) {
            // Delete the actual file from storage
            if (Storage::exists('public/' . $file->dokumen)) {
                Storage::delete('public/' . $file->dokumen);
            }

            // Delete the pivot table entry
            $laporan->files()->detach($file->id);

            // Delete the file record from the database
            $file->delete();
        }

        // Delete the laporan itself
        $laporan->delete();

        return redirect()->back()->with('success', 'Laporan and its files have been deleted.');
    }
}

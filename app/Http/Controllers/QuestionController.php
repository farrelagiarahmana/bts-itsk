<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Question;

class QuestionController extends Controller
{
    public function submitQuestion(Request $request)
    {

        $request->validate([
            'question_content' => 'required|string',
        ]);

        Question::create([
            'id_userqa' => auth()->id(), 
            'question_content' => $request->question_content,
        ]);

        return redirect()->back()->with('success', 'Pertanyaan berhasil diajukan.');
    }

    public function kirimJawaban(Request $request, Question $question)
    {
        $request->validate([
            'jawaban' => 'required|string',
        ]);

        $question->answer_content = $request->jawaban;
        $question->is_answered = true;
        $question->admin_id = Auth::id();
        $question->save();

        return redirect()->back()->with('success', 'Pertanyaan berhasil dijawab.');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\File;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function adminProfile()
    {
        $title = 'Profile Admin';
        return view("admin_profile", compact('title'));
    }

    public function dosenProfile()
    {
        $title = 'Profile Dosen';
        return view('dosen_profile', compact('title'));
    }

    public function index()
    {
        return view("userprofile");
    }


    public function store(Request $request)
    {
        $user = auth()->user();
        $userModel = User::find($user->id);

        $request->validate([
            'nama'      => 'required|string',
            'email'     => 'required|unique:users,email,' . $user->id,
            'nim'       => 'required|unique:users,nim,' . $user->id,
            'avatar'    => 'image',
        ]);

        if ($request->has('delete_avatar') && $userModel->avatar !== 'default.png') {
            $avatarPath = public_path('avatars/' . $userModel->avatar);
            if (File::exists($avatarPath)) {
                File::delete($avatarPath);
            }
            $userModel->avatar = null;
        }

        $input = $request->only(['nama', 'email', 'nim', 'avatar']);

        if ($request->hasFile('avatar')) {
            $avatarName = time() . '.' . $request->avatar->getClientOriginalExtension();
            $request->avatar->move(public_path('avatars'), $avatarName);

            $input['avatar'] = $avatarName;
        } else {
            unset($input['avatar']);
        }

        $userModel->update($input);

        return back()->with('success', 'Profile berhasil diupdate');
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|min:7|confirmed',
        ]);

        $user = User::find(Auth::user()->id);

        if (!Hash::check($request->current_password, $user->password)) {
            return back()->with('error', 'Password saat ini tidak sesuai.');
        }

        $user->password = Hash::make($request->new_password);
        $user->save();

        return back()->with('success', 'Password berhasil diubah.');
    }
}

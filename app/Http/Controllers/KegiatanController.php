<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kegiatan;
use App\Models\Provinsi;

class KegiatanController extends Controller
{
    public function indexForm()
    {
        $kegiatan = Kegiatan::all();
        return view("admin.kegiatan.kegiatan_index", compact("kegiatan"));
    }

    public function createForm()
    {
        $provinsi = Provinsi::all();
        return view("admin.kegiatan.kegiatan_create", compact("provinsi"));
    }

    public function storeKegiatan(Request $request) // CREATE UNTUK UJI COBA CREATE, AGAR TIDAK PERLU PINDAH-PINDAH USER
    {
        $request->validate([
            'id_provinsi' => 'required',
            'tanggal_kegiatan' => 'required|date',
            'sekolah' => 'required',
            'status_promosi' => 'required|in:Diterima,Diproses,Ditolak',
            'catatan_promosi' => 'nullable',
        ]);

        Kegiatan::create($request->all());

        return redirect()->route('admin.kegiatan.index')->with('success', 'Kegiatan berhasil ditambahkan.');
    }

    public function editForm(Kegiatan $kegiatan)
    {
        $provinsi = Provinsi::all();
        return view('admin.kegiatan.kegiatan_edit', compact('kegiatan', 'provinsi'));
    }

    public function updateKegiatan(Request $request, Kegiatan $kegiatan)
    {
        $request->validate([
            'status_promosi' => 'required|in:Diterima,Diproses,Ditolak',
            'catatan_promosi' => 'nullable',
        ]);

        $kegiatan->update($request->all());
        return redirect()->route('admin.kegiatan.index')->with('success', 'Kegiatan berhasil diperbarui.');
    }

    public function destroyKegiatan(Kegiatan $kegiatan)
    {
        $kegiatan->delete();

        return redirect()->route('admin.kegiatan.index')->with('success', 'Kegiatan berhasil dihapus.');
    }
}

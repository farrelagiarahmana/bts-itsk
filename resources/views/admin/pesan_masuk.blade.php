@extends('layouts.main')

@section('content')
    <h1>Pesan Masuk</h1>

    <div class="pesan-list">
        @foreach($questions as $question)
            <div class="pesan-item">
                <div class="sender-info">
                    <span>{{ $question->user->nama }}</span>
                </div>
                <button class="btn btn-info" data-toggle="modal" data-target="#infoModal{{ $question->id }}">Info</button>
                <div class="pesan-content">
                    {{ $question->question_content }}
                </div>
                <!-- Form untuk mengirimkan jawaban -->
                <form method="POST" action="{{ route('kirim.jawaban', ['question' => $question->id]) }}">
                    @csrf
                    <!-- Kolom Jawaban -->
                    <div class="form-group mt-2">
                        <label for="jawaban{{ $question->id }}">Jawaban:</label>
                        <textarea class="form-control" name="jawaban" id="jawaban{{ $question->id }}" rows="3"></textarea>
                    </div>
                    <!-- Button Kirim -->
                    <button type="submit" class="btn btn-primary">Kirim</button>
                </form>
                <!-- Modal -->
                <div class="modal fade" id="infoModal{{ $question->id }}" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="infoModalLabel">Info Pengguna</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Nama: {{ $question->user->nama }}</p>
                                <p>NIM: {{ $question->user->nim }}</p>
                                <p>Email: {{ $question->user->email }}</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

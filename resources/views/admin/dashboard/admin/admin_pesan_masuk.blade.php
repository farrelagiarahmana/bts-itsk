@extends('admin.dashboard.layouts.main')

@php
    $title = 'Admin';
@endphp

@section('title')
    Dashboard Pesan Masuk
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <style>
        .pesan {
            background-color: rgb(250, 250, 250);
        }

        .pesan:hover {
            background-color: rgb(240, 240, 240);
        }
    </style>

    <!-- Button trigger modal -->
    {{-- <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal"
    style="margin-top: 100px; margin-left: 10px">
        Launch demo modal
    </button> --}}

    <!-- Modal -->
    {{-- <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"
    style="top: 35%">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div> --}}

    <h1 class="fs-4 fw-bold mx-3 mb-3" style="margin-top: 100px">Pesan Masuk</h1>
    <div class="col-12 border-top">
        {{-- Pesan / Tes Modal --}}
        {{-- <div data-bs-toggle="modal" data-bs-target="#exampleModal" class="pesan d-flex text-dark text-decoration-none px-2 py-3 border-bottom">
            <div class="col-2 col-sm-1">
                <img src="{{ asset('img/profile.png') }}" alt="Foto Profil" class="d-block mx-auto" style="width: 45px">
            </div>
            <div class="col-8 col-sm-9 px-2">
                <h1 class="fs-6">M. Revanza Yuzar</h1>
                <p class="m-0">Assalamualaikum min izin bertanya, kenapa pada saat saya ingin
                    mendownload sertifikat kok error ya? Mohon solusinya ...</p>
            </div>
            <div class="col-2 d-flex flex-column justify-content-start align-items-center">
                <small class="text-center">26 Februari 2024</small>
                <span class="text-light"
                    style="background-color: #19D242; padding: 0 8px; margin-top: 7px; border-radius: 50%">
                    1
                </span>
            </div>
        </div> --}}

        {{-- Pesan --}}
        <div class="pesan d-flex text-dark text-decoration-none px-2 py-3 border-bottom">
            <div class="col-2 col-sm-1">
                <img src="{{ asset('img/profile.png') }}" alt="Foto Profil" class="d-block mx-auto" style="width: 45px">
            </div>
            <div class="col-8 col-sm-9 px-2">
                <h1 class="fs-6">M. Revanza Yuzar</h1>
                <p class="m-0">Assalamualaikum min izin bertanya, kenapa pada saat saya ingin
                    mendownload sertifikat kok error ya? Mohon solusinya ...</p>
            </div>
            <div class="col-2 d-flex flex-column justify-content-start align-items-center">
                <small class="text-center">26 Februari 2024</small>
                <span class="text-light"
                    style="background-color: #19D242; padding: 0 8px; margin-top: 7px; border-radius: 50%">
                    1
                </span>
            </div>
        </div>
    </div>
@endsection
@extends('admin.dashboard.layouts.main')

@php
    $title = 'Admin - Pendaftar';
@endphp

@section('title')
    Dashboard Kegiatan Terselesaikan
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="box-menu d-flex flex-column align-items-center flex-md-row justify-content-md-evenly mb-5"
        style="margin-top: 125px;">
        <a href="{{ url('/dashboard-pesan-masuk') }}"
            class="col-9 col-md-3 d-flex mb-5 mb-md-0 text-dark text-decoration-none shadow-sm">
            <div class="col-4 d-flex justify-content-center py-2" style="background-color: #2F88FF;">
                <i class="bi bi-envelope text-light" style="font-size: 36px;"></i>
            </div>
            <div class="col-8 d-flex align-items-center">
                <p class="mx-3 mb-0">Pesan Masuk<br><span class="fw-bold">305</span></p>
            </div>
        </a>

        <a href="{{ route('dash.pendaftar') }}"
            class="col-9 col-md-3 d-flex mb-5 mb-md-0 text-dark text-decoration-none shadow-sm">
            <div class="col-4 d-flex justify-content-center py-2" style="background-color: #19D242;">
                <i class="bi bi-list-ol text-light" style="font-size: 36px;"></i>
            </div>
            <div class="col-8 d-flex align-items-center">
                <p class="mx-3 mb-0">Pendaftar<br><span class="fw-bold">{{ $jumlahDiproses }}</span></p>
            </div>
        </a>

        <a href="{{ route('dash.selesai') }}"
            class="col-9 col-md-3 d-flex mb-5 mb-md-0 text-dark text-decoration-none shadow-sm">
            <div class="col-4 d-flex justify-content-center py-2" style="background-color: #FB8700;">
                <i class="bi bi-clipboard-check text-light" style="font-size: 36px;"></i>
            </div>
            <div class="col-8 d-flex align-items-center">
                <p class="mx-3 mb-0">Telah Selesai<br><span class="fw-bold">{{ $jumlahDiterima }}</span></p>
            </div>
        </a>
    </div>

    <div class="col-11 mx-auto mb-5 border overflow-hidden"
        style="background-color: rgb(255, 255, 255); font-size: 13px; border-radius: 10px">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center p-4">
            <h1 class="fs-5 mb-3 mb-sm-0">Data Kegiatan Terselesaikan</h1>
        </div>
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center text-secondary px-4">
            <form action="#" method="post" class="mb-3 mb-sm-0">
                <label for="show">Show</label>
                <input type="number" name="show" id="show" value="10" class="border border-2"
                    style="width: 75px; border-radius: 5px;">
                <span>entries</span>
            </form>
            <form action="#" method="post" class="">
                <label for="search">Search :</label>
                <input type="text" name="search" id="search" class="border border-2 px-1"
                    style="width: 200px; border-radius: 5px;">
            </form>
        </div>

        {{-- Table --}}
        <div class="table-responsive mt-4">
            <table class="table table-hover">
                <thead class="table-light border-top border-bottom">
                    <tr>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">Nama Ketua</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">Nama Dosen</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">Sekolah</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">Tanggal</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">Status</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($kegiatans as $kegiatan)
                        <tr>
                            <td class="text-secondary text-center px-3 text-nowrap">
                                @foreach ($kegiatan->users()->where('jabatan', 'Ketua')->get() as $anggota)
                                    {{ $anggota->nama }}
                                @endforeach
                            </td>
                            <td class="text-secondary text-center px-3 text-nowrap">
                                @foreach ($kegiatan->users()->where('jabatan', 'Dosen')->get() as $anggota)
                                    {{ $anggota->nama }}
                                @endforeach
                            </td>
                            <td class="text-secondary text-center px-3 text-nowrap">{{ $kegiatan->sekolah }}</td>
                            <td class="text-secondary text-center px-3 text-nowrap">{{ $kegiatan->tanggal_kegiatan }}</td>
                            <td class="text-secondary text-center px-3 text-nowrap">
                                <span class="badge text-bg-success fw-normal pb-2"
                                    style="font-size: 13px">{{ $kegiatan->status_promosi }}</span>
                            </td>
                            <td class="text-center px-3 text-nowrap">
                                <form action="#" method="post" class="">
                                    <button type="submit" name="lihat"
                                    class="btn btn-outline-light text-secondary fs-5 mx-1">
                                    <i class="bi bi-eye"></i>
                                </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{-- Table End --}}

        <div class="d-flex flex-column flex-md-row justify-content-between align-items-center text-secondary p-4">
            <div>Showing <span>{{ $kegiatans->firstItem() }}</span> to <span>{{ $kegiatans->lastItem() }}</span> of
                <span>{{ $kegiatans->total() }}</span> entries</div>
            <nav class="mt-5 mt-md-0">
                <ul class="pagination mb-0">
                    @if ($kegiatans->onFirstPage())
                        <li class="page-item disabled">
                            <span class="page-link text-secondary">
                                <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                            </span>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link text-secondary" href="{{ $kegiatans->previousPageUrl() }}">
                                <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                            </a>
                        </li>
                    @endif

                    @foreach ($kegiatans->getUrlRange(1, $kegiatans->lastPage()) as $page => $url)
                        <li class="page-item {{ $page == $kegiatans->currentPage() ? 'active' : '' }}">
                            <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                        </li>
                    @endforeach

                    @if ($kegiatans->hasMorePages())
                        <li class="page-item">
                            <a class="page-link text-secondary" href="{{ $kegiatans->nextPageUrl() }}">
                                Next <i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                            </a>
                        </li>
                    @else
                        <li class="page-item disabled">
                            <span class="page-link text-secondary">
                                Next <i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                            </span>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
@endsection
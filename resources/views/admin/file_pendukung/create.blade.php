@extends('admin.dashboard.layouts.main')

@section('title', 'Tambah File Pendukung')

@php
    $title = 'Tambah File Pendukung';
@endphp

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center align-items-center" style="height: 100vh;">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header fs-5 mb-0">Tambah File Pendukung</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ route('file_pendukung.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group pt-4 px-3">
                            <label for="nama">Nama File:</label>
                            <input type="text" class="form-control" id="nama" name="nama" required>
                        </div>
                        <div class="form-group pt-4 px-3" style="margin-top: 10px;">
                            <label for="jenis">Jenis File:</label>
                            <input type="text" class="form-control" id="jenis" name="jenis" required>
                        </div>
                        <div class="form-group pt-4 px-3" style="margin-top: 10px;">
                            <label for="file">Upload File:</label>
                            <input type="file" class="form-control" id="file" name="file" required accept=".pdf,.ppt,.pptx,.doc,.docx">
                        </div></br></br>
                        <br>
                         <div class="text-center pt-5">
                            <button type="submit" class="btn" style="color: white; background-color: black;">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
